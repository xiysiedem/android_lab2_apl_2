package com.example.xd.lab2cz2apl2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button)findViewById(R.id.open_another_application);
        Button calendar = (Button)findViewById(R.id.calendar);
        Button www = (Button)findViewById(R.id.www);
        Button time = (Button)findViewById(R.id.time);
        assert button != null;
        assert calendar != null;
        assert www != null;
        assert time != null;
        button.setOnClickListener(this);
        calendar.setOnClickListener(this);
        www.setOnClickListener(this);
        time.setOnClickListener(this);


    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Context context = getApplicationContext();
        String string = "Znów jestem!";
        int duration = Toast.LENGTH_SHORT;
        Toast.makeText(context, string, duration).show();
    }
    public void onClick(View v) {
        Context context = getApplicationContext();
        switch (v.getId()){
            case R.id.open_another_application:
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.example.xd.lab2cz2");
                startActivity(launchIntent);
                break;
            case R.id.calendar:
                Intent intent = new Intent(this,calendar.class);
                startActivity(intent);
                break;
            case R.id.www:
                intent = new Intent(this,www.class);
                startActivity(intent);
                break;
            case R.id.time:
                intent = new Intent(this,time.class);
                startActivity(intent);
                break;
        }


    }
    private static long back_pressed;

    @Override
    public void onBackPressed()
    {
        if (back_pressed + 2000 > System.currentTimeMillis())
            super.onBackPressed();
        else
            Toast.makeText(getBaseContext(), "Jeszcze tu wróce!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();
    }

}
