package com.example.xd.lab2cz2apl2;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class www extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_www);

        WebView webview = new WebView(this);
        setContentView(webview);
        webview.loadUrl("http://google.com/");
    }
}
